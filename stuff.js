
(function () {
// Specify the next thing at which the stuff needs to happen.
var older_granularity = 'minute';
var weekdayMap = [
    'none',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday',
];

    // My attempt at using react, too lazy to come up with build system.
    var CenterThing = React.createClass({
        displayName: 'CenterThing',
        getInitialState: function () {
            return { centers: [], };
        },
        render: function () {
            var builtCenters = [];
            var center;
            for (center of this.state.centers) {
                builtCenters.push(
                    React.createElement(
                        'dt',
                        {
                            key: center.key + '-dt',
                        },
                        React.createElement('a', {href: center.uri}, center.friendly)));
                builtCenters.push(React.createElement('dd', {key: center.key + '-dd'}, center.hours));
            }
            return React.createElement(
                'dl',
                null,
                builtCenters);
        },
    });
    var centerThing = ReactDOM.render(React.createElement(CenterThing, null), document.getElementById('center-hours'));

    var update_things = function () {
        var now = moment();
        var isoWeekday = now.isoWeekday();
        console.log(`is:${isoWeekday}`);
        var i;
        for (i = 0; i < weekdayMap.length; i++) {
            document.body.classList.toggle('currently-' + weekdayMap[i], isoWeekday === i);
        }
        
        jQuery('table td.time').map(function () {
            var e = jQuery(this);
            var now = moment(/*'1:32 PM', [ 'h:m a', 'H:m', ]*/);
            var rowTime = moment(e.text(), [ 'h:m a', 'H:m', ]);
            console.log(`checking ${e.text()}`);
            jQuery(e).closest('tr').toggleClass('current', now.isSameOrAfter(rowTime) && now.isSameOrBefore(rowTime.add(30, 'minutes')));
        });

        jQuery.get('api.cgi').then(function (data) {
            var centers = [];
            for (var centerId of data.centersOrder) {
                var center = data.centers[centerId];
                center.key = centerId;
                centers.push(center);
            }
            centerThing.setState({centers: centers});
        });
    };
    
    var things_to_links = [
        {r: /CS-232/, href: 'https://cs.calvin.edu/courses/cs/232/',},
        {r: /CS-352/, href: 'https://cs.calvin.edu/courses/cs/352/',},
        {r: /CS-342/, href: 'https://cs.calvin.edu/courses/cs/342',},
        {r: /CS-384/, href: 'http://moodle.calvin.edu/course/view.php?id=38897',},
        {r: /GEOG-110/, href: 'http://moodle.calvin.edu/course/view.php?id=39065',},
        {r: /CS-295/, href: 'https://cs.calvin.edu/courses/cs/x95/kvlinden/scheduleSpring.html',},
    ];
    jQuery('table td').map(function () {
        var e = jQuery(this);
        things_to_links.map(function (thing_to_link) {
            if (thing_to_link.r.test(e.text())) {
                var a = jQuery('<a/>', {href: thing_to_link.href, style: 'display: inline-block; text-decoration: none; color: black;',target:'_blank',});
                a.append(e[0].childNodes);
                e.append(a);
            }
        });
    });

// tate stolen from http://hg.ohnopub.net/hg/%CF%80time/file/1d328f296678/older.js and jQuery/rendering ripped out,  should be generified

var granularity_order = ['year', 'month', 'date', 'hour', 'minute', 'second', 'millisecond'];
var granularity_map = {year: 'years', month: 'months', date: 'days', hour: 'hours', minute: 'minutes', second: 'seconds', millisecond: 'milliseconds',};
var tate = function () {
                var now = moment();
    update_things();
    /*
                var rendered = selector_info.render(now, older_time);
                if (typeof rendered === 'string')
                    o.text(rendered);
                else
                {
                    o.empty();
                    o[0].appendChild(rendered);
                }
                */
                var cutting = false;
                var next = moment(now);
                granularity_order.map(function (granularity) {
                    if (cutting)
                        next.set(granularity, 0);
                    if (granularity === older_granularity)
                    {
                        next.add(1, granularity_map[granularity]);
                        cutting = true;
                    }
                });
                setTimeout(tate, Math.max(1000, next.diff(now)));
            };
            tate();
})();
