#!/usr/bin/env node
/* -*- mode: js; -*- */
'use strict';

var memoryCache = require('memory-cache');
var request = require('request');
var htmlparser = require('htmlparser2');

var cacheTime = 16000;

/**
 * Retrieve a cached value as a Promise or define a
 * cache entry if it doesn't exist yet. If the factory is
 * a Promise and gets rejected the value is cleared immediately
 * but all concurrent requests for an incomplete promise will
 * share the same Promise.
 */
var getCached = function (key, factory) {
    return memoryCache.get(key) || memoryCache.put(key, Promise.resolve(factory()).catch(function () { memoryCache.del(key); }), cacheTime);
};
exports.getCached = getCached;

var requestPromise = function () {
    var requestArguments = Array.prototype.slice.call(arguments);
    return new Promise(function (resolve, reject) {
        request.apply(this, requestArguments.concat(function (error, response, body) {
            if (error) {
                reject(error);
            } else {
                resolve({
                    response: response,
                    body: body,
                });
            }
        }));
    });
};
exports.requestPromise = requestPromise;

var getCenterHours = function (uri) {
        return requestPromise(uri).then(function (result) {
            var expectHours = false;
            var hours = 'Unknown';
            var parser = new htmlparser.Parser({
                decodeEntities: true,
                ontext: function (text) {
                    if (expectHours) {
                        text = text.trim();
                        if (text.length > 2) {
                            hours = text;
                            expectHours = false;
                        }
                    } else if (/today.*hour/i.test(text)) {
                        expectHours = true;
                    }
                },
            });
            parser.write(result.body);
            parser.end();
            return hours;
        });
};

var getData = function () {
    return getCached('data', function () {
        var centers = {
            climbing: {
                friendly: 'Climbing Wall',
                uri: 'https://calvin.edu/directory/places/calvin-climbing-center',
            },
            pool: {
                friendly: 'Pool',
                uri: 'https://calvin.edu/directory/places/venema-aquatic-center',
            },
            gym: {
                friendly: 'Gym',
                uri: 'https://calvin.edu/directory/places/venema-aquatic-center',
            },
        };
        var centersOrder = [
            'gym',
            'pool',
            'climbing',
        ];
        var centerId;
        var promises = [];
        var doGet = function (centerId) {
            return getCenterHours(centers[centerId]).then(function (hours) {
                centers[centerId].hours = hours;
            });
        };
        for (centerId in centers) {
            promises.push(doGet(centerId));
            if (!centersOrder.indexOf(centerId) === -1) {
                centersOrder.push(centerId);
            }
        }
        return Promise.all(promises).then(function () {
            return {
                centersOrder: centersOrder,
                centers: centers,
            };
        });
    });
};
exports.getData = getData;

if (!module.parent) {
    require('node-fastcgi').createServer(function (req, res) {
        getData().then(function (data) {
            res.writeHead(
                200,
                {
                    'Content-Type': 'application/json',
                });
            res.end(JSON.stringify(data));
        }).catch(function (e) {
            res.writeHead(
                500,
                {
                    'Content-Type': 'text/plain',
                });
            res.end(String((e || {}).message || e));
        });
    }).listen();
}
